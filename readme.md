# Invoice Module Import!

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

# Description
1. The functionally, is get the Vendor or Account from Xero.com, and storage in JSON files, the system have changelog from Syncs and backup from old files.
2. The system have Interface to interact with user, have 4 options, list from accounts and vendor, list/changelog from old files vendors and accounts.
3. The folder docs contains this file in TXT and PDF to SYSTEM FLOW.
4. If you want use docker and you don't have a docker environment, you need download here: https://www.docker.com/get-started
After that, you can run the docker commands.
	- Build the project: `docker build -t invoice-module .` 
	- and after, you run with: `docker run -p 8080:80 -d invoice-module`
	- Or you can download and run directly the InvoiceModuleApp image from Docker Hub: docker run -p 8080:80 -d alexmaramaldo/invoice-module
	- You access in url `http://localhost:8080`

> (*) If you put your custom certificates from Xero, you need build again the Docker Image, command example: `docker build -t invoice-module /LOCAL/PATH/TO/PROJECT`

# Requirements if you need create a custom server:
If you need put the project inside linux server, you need install APACHE 2 or NGINX or Similar, but you need PHP 7.2 and sub modules.
Example command with apache::
```sh
$ sudo apt-get update && apt-get -y upgrade && apt-get -y install \
  apache2 php7.2 libapache2-mod-php7.2 curl php7.2-curl php7.2-simplexml && a2enmod php7.2
```
And point the root directory to folder www

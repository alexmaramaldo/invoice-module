<?php include_once("header.php") ?>

<?php
    include_once("setup/changelog.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo strtoupper($type) ?>
            <small>all <?php echo $type ?> files from Xero</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Files</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo $thelist; ?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

<?php include_once("footer.php") ?>
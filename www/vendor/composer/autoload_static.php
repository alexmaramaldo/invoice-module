<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitaef461a3bb83c511ddc6cbb649149efa
{
    public static $prefixLengthsPsr4 = array (
        'X' => 
        array (
            'XeroPHP\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'XeroPHP\\' => 
        array (
            0 => __DIR__ . '/..' . '/calcinai/xero-php/src/XeroPHP',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitaef461a3bb83c511ddc6cbb649149efa::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitaef461a3bb83c511ddc6cbb649149efa::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}

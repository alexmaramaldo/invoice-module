<?php

$config = [

    'xero' => [
        // API versions can be overridden if necessary for some reason.
        //'core_version'     => '2.0',
        //'payroll_version'  => '1.0',
        //'file_version'     => '1.0'
    ],
    'oauth' => [
        'callback'    => 'oob',
        'consumer_key'      => 'K78JSUIUBR4ZTVPAFNP5JVIUMQIDC0',
        'consumer_secret'   => '2ZONF2VPXFVLCYOSRJXUO99YX7G8F2',
        //If you have issues passing the Authorization header, you can set it to append to the query string
        //'signature_location'    => \XeroPHP\Remote\OAuth\Client::SIGN_LOCATION_QUERY
        //For certs on disk or a string - allows anything that is valid with openssl_pkey_get_(private|public)
        'rsa_private_key'  => "file://certs/privatekey.pem",
        'rsa_public_key'   => "file://certs/public_privatekey.pfx"
    ],
    //These are raw curl options.  I didn't see the need to obfuscate these through methods
    'curl' => [
        CURLOPT_USERAGENT   => 'XeroPHP Test App',
        //Only for partner apps - unfortunately need to be files on disk only.
        //CURLOPT_CAINFO          => 'certs/ca-bundle.crt',
        //CURLOPT_SSLCERT         => 'certs/entrust-cert-RQ3.pem',
        //CURLOPT_SSLKEYPASSWD    => '1234',
        //CURLOPT_SSLKEY          => 'certs/entrust-private-RQ3.pem'
    ]

];

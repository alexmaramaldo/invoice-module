<?php
use XeroPHP\Models\Accounting\Contact;

require_once 'vendor/autoload.php';

$type = "vendors";
if (strpos($_GET['type'], "accounts") !== false)  {
    $type = "accounts";
}

$dir = "./database";

$thelist = "";
if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle)))
    {
        if ($file != "." && $file != ".." && strrpos($file, $type) !== false)
        {
            $f = explode("-", $file);

            if((int)$f[0] > 0 && is_int((int)$f[0])) {
                $thelist .= '<li><a href="./database/'.$file.'" target="_blank">'.$file.'</a> at '.date('Y-m-d H:i:s', strtotime($f[0])).'</li>';
            } else {
                $fileData = @json_decode(file_get_contents("./database/".$type.".json"))->updated_at;
                $string_date = ($fileData) ? " at " : "";
                $string_date .= $fileData;
                $thelist .= '<li><a href="./database/'.$file.'" target="_blank">current</a> '.$string_date.'</li>';
            }
        }
    }
    closedir($handle);
}

?>
<?php

$error = false;
if(isset($_GET["type"])) {
    if($_GET["type"] != 'vendors' && $_GET["type"] != 'accounts') {
        $error = true;
    }
} else {
    $error = true;
}
if($error) {
    header("Location: /");
}

use XeroPHP\Models\Accounting\Account;
use XeroPHP\Models\Accounting\Contact;

require_once '../vendor/autoload.php';
include_once './xero.php';

$type = $_GET["type"];
$xero = new \XeroPHP\Application\PrivateApplication($config);

$model = null;

if($type == "vendors") {
    $objects = $xero->load(Contact::class)->execute();
} else {
    $objects = $xero->load(Account::class)->execute();
}

$data = [];
$current_date = date('Y-m-d H:i:s');

foreach ($objects as $tmp) {
    $tmp = json_decode(json_encode($tmp));
    $attributes = get_object_vars($tmp);
    $object = new \stdClass();
    foreach($attributes as $attribute => $value) {
        $object->{strtolower($attribute)} = $value;
    }

    $data[] = $object;
}

$database_url = "../database/".$type.".json";

if($content = @file_get_contents($database_url)){
    if(isset($content) && strlen(trim($content)) > 0) {
        $dateFromFile = json_decode($content)->updated_at;
        $database_url_old = "../database/".date('YmdHis', strtotime($dateFromFile))."-".$type.".json";
        $database_copy = fopen($database_url_old, "w") or die("Unable to open file!");
        fwrite($database_copy, $content);
        fclose($database_copy);
    }
}

$database = fopen($database_url, "w") or die("Unable to open file!");
$content = [
    "updated_at" => $current_date,
    "data" => $data
];
$json_formatted = json_encode($content);
fwrite($database, $json_formatted);
fclose($database);

header('Location: /page-'.$type.'.php');



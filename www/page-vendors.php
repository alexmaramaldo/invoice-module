<?php include_once("header.php") ?>

<?php

use XeroPHP\Models\Accounting\Contact;

require_once 'vendor/autoload.php';

$vendors_database_url = "./database/vendors.json";

$vendors = (object) json_decode(@file_get_contents($vendors_database_url));


?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Vendors
            <small>all vendors from Xero</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title" style="height: 50px; !important;"><?php if(@$vendors->updated_at) { ?>Updated at: <?php } echo @$vendors->updated_at ?></h3>
                        <div style="width: 400px; position: relative; height: 2px; float:right; margin-right: -145px;">
                            <a href="./setup/update.php?type=vendors"><button style="float: left; width: 130px; margin: 0;" type="button" class="btn btn-block btn-primary btn">Sync with Xero</button></a>
                            <a href="./database/vendors.json" target="_blank"><button style="float: left; width: 120px; margin: 0 0 0 5px;" type="button" class="btn btn-block btn-success btn">View JSON</button></a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(@$vendors->data) foreach($vendors->data as $vendor) { ?>
                                <tr>
                                    <td><?php echo $vendor->name ?></td>
                                </tr>
                            <?php } ?>

                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

<?php include_once("footer.php") ?>
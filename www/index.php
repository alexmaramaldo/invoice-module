<?php include_once("header.php") ?>

<?php

use XeroPHP\Models\Accounting\Contact;

require_once 'vendor/autoload.php';

$accounts_database_url = "./database/accounts.json";

$accounts = @json_decode(file_get_contents($accounts_database_url))->data;

$vendors_database_url = "./database/vendors.json";

$vendors = @json_decode(file_get_contents($vendors_database_url))->data;

?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo count($vendors) ?></h3>

                        <p>Vendors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="./page-vendors.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo count($accounts) ?></h3>

                        <p>Accounts</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="./page-accounts.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->


<?php include_once("footer.php") ?>